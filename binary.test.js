const binary = require("./binary");

//to check if the correct index value is returned or not
test(`should return element index in the array`, () => {
  expect(binary([12, 15, 17, 22, 28, 39, 45, 48, 79], 79)).toBe(9);
});

//to check if the -1 is returned when no match found
test(`should return -1 when no match found`, () => {
  expect(binary([12, 15, 17, 22, 28, 39, 45, 48, 79], 179)).toBe(-1);
});

//to check if the array is sorted or not
test(`should give a sorted array only `, () => {
  expect(() => {
    binary([12, 54, 17, 22, 28, 39, 45, 48, 79], 179);
  }).toThrowError("array is not sorted");
});
