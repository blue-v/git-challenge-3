function fizzbuzz(number) {
  if (number % 3 == 0 && number % 5 == 0) {
    return "FizzBuzz";
  } else if (number % 3 == 0) {
    return "Fizz";
  } else if (number % 5 == 0) {
    return "Buzz";
  } else {
    return number;
  }
}
for (let i = 0; i < 40; i++) {
  fizzbuzz(i);
}

module.exports = fizzbuzz;
