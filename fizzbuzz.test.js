// const fizzbuzz = require("./fizzbuzz");

// test("returns 1 for the number 1", () => {
//   expect(fizzbuzz(1)).toEqual(1);
// });

// test("returns 2 for the number 2", () => {
//   expect(fizzbuzz(2)).toEqual(2);
// });

// test('returns "Fizz" for the number 3', () => {
//   expect(fizzbuzz(3)).toEqual("Fizz");
// });

// test("returns 4 for the number 4", () => {
//   expect(fizzbuzz(4)).toEqual(4);
// });

// test('returns "Buzz" for the number 5', () => {
//   expect(fizzbuzz(5)).toEqual("Buzz");
// });

// test('returns "Fizz" for the number 6', () => {
//   expect(fizzbuzz(6)).toEqual("Fizz");
// });

// test("returns 8 for the number 8", () => {
//   expect(fizzbuzz(8)).toEqual(8);
// });

// test('returns "FizzBuzz" for the number 15', () => {
//   expect(fizzbuzz(15)).toEqual("FizzBuzz");
// });

// test('returns "Fizz" for the number 9', () => {
//   expect(fizzbuzz(9)).toEqual("Fizz");
// });

// test('returns "Buzz" for the number 10', () => {
//   expect(fizzbuzz(10)).toEqual("Buzz");
// });

// test('returns "FizzBuzz" for the number 30', () => {
//   expect(fizzbuzz(30)).toEqual("FizzBuzz");
// });

// test("returns 7 for the number 7", () => {
//   expect(fizzbuzz(7)).toEqual(7);
// });

const fizzBuzz = require("./fizzbuzz");

test("Number divisible by both 3 and 5 returns FizzBuzz", () => {
  expect(fizzBuzz(15)).toEqual("FizzBuzz");
});

test("Number only divisible by 3 returns Fizz", () => {
  expect(fizzBuzz(9)).toEqual("Fizz");
});

test("Number only divisible by 5 returns Buzz", () => {
  expect(fizzBuzz(20)).toEqual("Buzz");
});
test("Number not divisible by 3 or 5 returns Buzz", () => {
  expect(fizzBuzz(2)).toEqual(2);
});
