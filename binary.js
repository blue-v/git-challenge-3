const binary = (array, key) => {
  const sarray = [...array].sort((a, b) => a - b);

  for (let i = 0; i < array.length; i++) {
    if (array[i] !== sarray[i]) throw new Error("array is not sorted");
  }

  let lb = 0;
  let ub = array.length;

  while (lb <= ub) {
    const mid = Math.floor((lb + ub) / 2);
    if (key == array[mid]) return mid + 1;
    else if (key > array[mid]) lb = mid + 1;
    else ub = mid - 1;
  }
  return -1;
};
module.exports = binary;
